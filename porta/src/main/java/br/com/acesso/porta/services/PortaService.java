package br.com.acesso.porta.services;

import br.com.acesso.porta.models.Porta;
import br.com.acesso.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {
    @Autowired
    private PortaRepository portaRepository;

    public Porta salvarPorta(Porta porta){
        Porta objetoPorta = portaRepository.save(porta);
        return objetoPorta;
    }

    public Porta buscarPortaPeloId(int id){
        Optional<Porta> portaOptional = portaRepository.findById(id);
        if(portaOptional.isPresent()){
            Porta porta = portaOptional.get();
            return porta;
        }else{
            throw new RuntimeException("A porta "+ id +" não foi encontrado");
        }
    }
}