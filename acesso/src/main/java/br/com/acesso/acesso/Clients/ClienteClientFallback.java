package br.com.acesso.acesso.Clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente getById(int id) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço CLIENTE indisponível no momento. Tente novamente mais tarde.");
    }
}

