package br.com.acesso.acesso.Clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PORTA", configuration = PortaClientConfiguration.class)
public interface PortaClient {
    @GetMapping("/porta/{porta_id}")
    Porta getById(@PathVariable int id);
}

