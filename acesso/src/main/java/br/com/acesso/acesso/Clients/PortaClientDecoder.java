package br.com.acesso.acesso.Clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new PortaNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
