package br.com.acesso.acesso.services;


import br.com.acesso.acesso.Clients.Cliente;
import br.com.acesso.acesso.Clients.ClienteClient;
import br.com.acesso.acesso.Clients.Porta;
import br.com.acesso.acesso.Clients.PortaClient;
import br.com.acesso.acesso.DTO.AcessoDTO;
import br.com.acesso.acesso.models.Acesso;
import br.com.acesso.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessoService {
    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    public AcessoDTO salvarAcesso(AcessoDTO acessoDTO) {
        Cliente cliente = clienteClient.getById(acessoDTO.getClienteId());
        Porta porta = portaClient.getById(acessoDTO.getPortaId());

        if ((cliente != null) && (porta != null)) {
                Acesso acessoNovo = new Acesso();
                acessoNovo.setClienteId(acessoDTO.getClienteId());
                acessoNovo.setPortaId(acessoDTO.getPortaId());
                acessoRepository.save(acessoNovo);
                return this.transformarAcessoEmDTO(acessoNovo);
        } else {
            throw new RuntimeException("Dados inválidos - Cliente "+ cliente.getIdCliente() +" ou Porta "+ porta.getIdPorta() +" não encontrados para salvar o acesso!");
        }
    }

    private AcessoDTO transformarAcessoEmDTO(Acesso acesso) {
        AcessoDTO acessoDTO = new AcessoDTO();

        acessoDTO.setPortaId(acesso.getPortaId());
        acessoDTO.setClienteId(acesso.getClienteId());

        return acessoDTO;
    }
    public Acesso getByIdClienteAndByIdPorta(int clienteId , int portaId){
        return acessoRepository.findByClienteIdAndPortaId(clienteId , portaId);
    }

}
