package br.com.acesso.acesso.Clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {
    @GetMapping("/cliente/{cliente_id}")
    Cliente getById(@PathVariable int id);
}

