package br.com.acesso.acesso.Clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta getById(int id) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço PORTA indisponível no momento. Tente novamente mais tarde.");
    }
}

