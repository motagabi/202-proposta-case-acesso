package br.com.acesso.acesso.repositories;

import br.com.acesso.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {
    Iterable<Acesso> findAllById(int id);

    Acesso findByClienteIdAndPortaId(int clienteId, int portaId);
}